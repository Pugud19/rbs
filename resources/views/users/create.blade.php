@extends('admin.index')

@section('content')
    <div class="content-wrapper">
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Form input User</h4>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="" method="POST" class="forms-sample" enctype="multipart/form-data">
                    @csrf
                  <button type="submit" name="proses" class="btn btn-primary mr-2">Submit</button>
                  <a href="" class="btn btn-warning text-white">Batal</a>
                </form>
              </div>
            </div>
          </div>
    </div>
    </div>
@endsection
