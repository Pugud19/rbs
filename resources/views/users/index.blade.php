@extends('admin.index')

@section('content')
@if(Auth::user()->role == 'admin')
<div class="content-wrapper">
    <div class="d-flex justify-content-between my-3">
        <h3>Data User</h3>
        <a class="btn btn-info btn-sm btn-icon-text mr-3" href="">
            Tambah
            <i class="typcn typcn-plus btn-icon-append"></i>
          </a>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="table-responsive pt-3">
            <table class="table table-striped project-orders-table">
              <thead>
                <tr>
                  <th class="ml-5">No</th>
                  <th>Kode Gedung</th>
                  <th>Nama Gedung</th>
                  <th>Alamat</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  {{-- <td>no</td>
                  <td>action</td>
                  <td>
                    <div class="d-flex align-items-center">
                      <button type="button" class="btn btn-success btn-sm btn-icon-text mr-3">
                        Edit
                        <i class="typcn typcn-edit btn-icon-append"></i>
                      </button>
                      <button type="button" class="btn btn-danger btn-sm btn-icon-text">
                        Delete
                        <i class="typcn typcn-delete-outline btn-icon-append"></i>
                      </button>
                    </div>
                  </td> --}}
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@else
@include('access')
@endif

@endsection
