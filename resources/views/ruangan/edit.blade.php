@extends('admin.index')

@section('content')
    <div class="content-wrapper">
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Form Edit Ruangan</h4>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('ruangan.update', $ruangan->id) }}" method="POST" class="forms-sample" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                  <div class="form-group row">
                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nama Ruangan</label>
                    <div class="col-sm-9">
                      <input type="text" name="nama" class="form-control" id="exampleInputUsername2" value="{{ $ruangan->nama }}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="exampleSelectGender"  class="col-sm-3 col-form-label">Kategori Ruangan</label>
                    <div class="col-sm-9">
                        <select class="form-control" id="exampleSelectGender" name="kategori_ruangan_id">
                            <option>-- Pilih Kategori Ruangan --</option>
                            @foreach ($kategori_ruangan as $kat)
                            <option value="{{ $kat->id }}" {{ ($kat->id == $ruangan->kategori_ruangan_id) ? 'selected' : '' }}>{{ $kat->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    </div>
                    <div class="form-group row">
                        <label for="exampleSelectGender"  class="col-sm-3 col-form-label">Gedung</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="exampleSelectGender" name="gedung_id">
                                <option>-- Pilih Kategori Gedung --</option>
                                @foreach ($gedung as $gd)
                                <option value="{{ $gd->id }}"  {{ ($gd->id == $ruangan->gedung_id) ? 'selected' : '' }}>{{ $gd->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        </div>
                  <div class="form-group row">
                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Lantai</label>
                    <div class="col-sm-9">
                        <input type="text" name="lantai" class="form-control" value="{{ $ruangan->lantai }}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Kapasitas</label>
                    <div class="col-sm-9">
                        <input type="text" name="kapasitas" class="form-control" value="{{ $ruangan->kapasitas }}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Foto 1</label>
                    <div class="col-sm-9">
                        <input type="file" name="foto1" class="form-control" >
                    </div>
                  </div>
                  <div class="form-check form-check-flat form-check-primary">
                  </div>
                  <button type="submit" name="proses" class="btn btn-primary mr-2">Ubah</button>
                  <a href="{{ url('ruangan')}}" class="btn btn-warning text-white">Batal</a>
                </form>
              </div>
            </div>
          </div>
    </div>
    </div>
@endsection
