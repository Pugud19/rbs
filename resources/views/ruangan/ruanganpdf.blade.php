<!DOCTYPE html>
<html>
<head>
    <title>Data Ruangan</title>
</head>
<body>
    <h3 align="center">Data Ruangan</h3>
    <table align="center" border="1" cellpadding="10" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Ruangan</th>
            <th>Kategori Ruangan</th>
            <th>Gedung</th>
            <th>Kapasitas</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($data as $dt)
            @php
                $i = 0
            @endphp
          <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $dt->nama }}</td>
            <td>{{ $dt->kategoriRuangan->nama }} </td>
            <td>{{ $dt->gedung->nama }} </td>
            <td>{{ $dt->kapasitas }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>

</body>
</html>
