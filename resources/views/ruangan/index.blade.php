@extends('admin.index')

@section('content')
@if(Auth::user()->role != 'peminjam')
<div class="content-wrapper">

    <div class="d-flex justify-content-between my-3">
        <h3>Data Ruangan</h3>
        <div>
            <a class="btn btn-info btn-sm btn-icon-text mr-3" href="{{ route('ruangan.create')}}">
                Tambah
                <i class="typcn typcn-plus btn-icon-append"></i>
              </a>
              <a href="{{ url('/generate-pdf') }}"  class="btn btn-success btn-sm btn-icon-text mr-3" title="Edit data ruangan">
                PDF
                <i class="typcn typcn-folder  btn-icon-append"></i>
              </a>
              <a href="{{ url('/ruangan-pdf') }}"  class="btn btn-success btn-sm btn-icon-text mr-3" title="Edit data ruangan">
                Donwload
                <i class="typcn typcn-download btn-icon-append"></i>
              </a>
              <a href="{{ url('/ruangan-pdf') }}"  class="btn btn-success btn-sm btn-icon-text mr-3" title="Edit data ruangan">
                Excel
                <i class="typcn typcn-download  btn-icon-append"></i>
              </a>
        </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="table-responsive pt-3">
            <table class="table table-striped project-orders-table">
              <thead>
                <tr>
                  <th class="ml-5">No</th>
                  <th>Nama Ruangan</th>
                  <th>Kategori Ruangan</th>
                  <th>Gedung</th>
                  <th>Kapasitas</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($ruangan as $r)

                <tr>
                  <td>{{ ++$i }}</td>
                  <td>{{ $r->nama }}</td>
                  <td>{{ $r->kategoriRuangan->nama }} </td>
                  <td>{{ $r->gedung->nama }} </td>
                  <td>{{ $r->kapasitas }}</td>
                  <td>
                    <div class="d-flex align-items-center">
                      <a href="{{ route('ruangan.show', $r->id) }}" title="Show data ruangan"  class="btn btn-info btn-sm btn-icon-text mr-3">
                        View
                        <i class="typcn typcn-eye btn-icon-append"></i>
                      </a>
                        <a href="{{ route('ruangan.edit', $r->id) }}"  class="btn btn-success btn-sm btn-icon-text mr-3" title="Edit data ruangan">
                        Edit
                        <i class="typcn typcn-edit btn-icon-append"></i>
                      </a>
                      {{-- <form action="{{ route('ruangan.destroy', $r->id)}}" method="POST">
                        @csrf
                        @method('DELETE') --}}
                        <a type="submit" class="btn btn-danger btn-sm btn-icon-text button delete-confirm" title="Hapus data" href="/ruangan-delete/{{$r->id}}">
                            Delete
                            <i class="typcn typcn-delete-outline btn-icon-append"></i>
                          </a>

                      {{-- </form> --}}
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @else
  @include('access')
  @endif
@endsection
