@extends('landingpage.index')

@section('content')
<section class="roberto-cta-area">
    <div class="container">
        <div class="cta-content bg-img bg-overlay jarallax" style="background-image: url(img/bg-img/9.jpg);">
            <div class="row align-items-center">
                <div class="col-12 col-md-7">
                    <div class="cta-text mb-50">
                        <h2>Terima Kasih Telah Registrasi!</h2>
                        <h6>Mohon tunggu untuk aprovval akun anda</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
