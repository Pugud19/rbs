@extends('admin.index')

@section('content')
    <div class="content-wrapper">
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Form input Gedung</h4>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('gedung.store') }}" method="POST" class="forms-sample" enctype="multipart/form-data">
                    @csrf
                  <div class="form-group row">
                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Kode</label>
                    <div class="col-sm-9">
                      <input type="text" name="kode" class="form-control" id="exampleInputUsername2" placeholder="Kode">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Nama Gedung</label>
                    <div class="col-sm-9">
                      <input type="text" name="nama" class="form-control" id="exampleInputEmail2" placeholder="Nama Gedung">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Alamat</label>
                    <div class="col-sm-9">
                      <textarea type="text" name="alamat" class="form-control" id="exampleInputMobile" placeholder="Alamat"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Foto</label>
                    <div class="col-sm-9">
                        <input type="file" name="foto" class="form-control" placeholder="foto">
                    </div>
                  </div>
                  <div class="form-check form-check-flat form-check-primary">
                  </div>
                  <button type="submit" name="proses" class="btn btn-primary mr-2">Submit</button>
                  <a href="{{ url('gedung')}}" class="btn btn-warning text-white">Batal</a>
                </form>
              </div>
            </div>
          </div>
    </div>
    </div>
@endsection
