@extends('landingpage.index')

@section('content')
    <!-- Our Room Area Start -->
    <section class="roberto-rooms-area">
        <div class="rooms-slides owl-carousel">
            <!-- Looping data Gedung -->
            @foreach ($gedungs as $gd)
            <div class="single-room-slide d-flex align-items-center">
                <!-- Thumbnail -->
                <div class="room-thumbnail h-100 bg-img" style="background-image: url({{ asset('img/gedung/'.$gd->foto)}});"></div>

                <!-- Content -->
                <div class="room-content">
                    <h2 data-animation="fadeInUp" data-delay="100ms">{{ $gd->nama }}</h2>
                    <ul class="room-feature" data-animation="fadeInUp" data-delay="500ms">
                        <li><span><i class="fa fa-check"></i> Kode</span> <span>: {{ $gd->kode }}</span></li>
                        <li><span><i class="fa fa-check"></i> Alamat</span> <span>: {{ $gd->alamat }}</span></li>
                    </ul>
                    <a href="#" class="btn roberto-btn mt-30" data-animation="fadeInUp" data-delay="700ms">View Details</a>
                </div>
            </div>
            @endforeach
        </div>
    </section>
    <!-- Our Room Area End -->
@endsection
