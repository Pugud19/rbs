<?php

namespace App\Http\Controllers;

use App\Models\Ruangan;
use App\Models\Gedung;
use App\Models\KategoriRuangan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PDF;

class RuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Tampil Data from Database
        $ruangan = Ruangan::latest()->paginate(5);

        return view('ruangan.index',compact('ruangan'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    public function generatePDF()
    {
        $data = [
            'title' => 'Test use PDF extention',
            'date' => date('m/d/Y')
        ];

        $pdf = PDF::loadView('ruangan/myPDF', $data);

        return $pdf->download('hasil_exporPDF.pdf');
    }

    function ruanganPDF(){
        $data = Ruangan::all();
        // dd($data);
        $pdf = PDF::loadView('ruangan/ruanganPDF', ['data'=>$data]);

        return $pdf->download(date('m/d/y').'_dataruangan.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // ambil seluruh data dari kategori-ruangan
        $kategori_ruangan = KategoriRuangan::all();
        // ambil seluruh data dari Gedung
        $gedung = Gedung::all();
        // tambah input ke form database
        return view('ruangan.create', compact('kategori_ruangan','gedung'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Proses input data ruangan with foto
        $request->validate([
            'nama' => 'required',
            'kategori_ruangan_id' => 'required',
            'gedung_id' => 'required',
            'kapasitas' => 'required|numeric',
            'lantai' => 'required|numeric',
            'foto1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:3068',
        ]);

        $input = $request->all();

        // logic for proses foto
        if ($image = $request->file('foto1')) {
            $destinationPath = 'img/ruangan';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['foto1'] = "$profileImage";
        }

        // input create gedung function
        // Ruangan::create($input);

        // kembali ke page
        // return redirect()->route('ruangan.index')
                        // ->with('success','Gedung created successfully.');

        try {
            Ruangan::create($input);

            return redirect()->route('ruangan.index')
                ->with('success', 'Ruangan Created successfully!');
        } catch (\Exception $e){
            return redirect()->back()
                ->with('error', 'Error during the creation!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ruangan  $ruangan
     * @return \Illuminate\Http\Response
     */
    public function show(Ruangan $ruangan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ruangan  $ruangan
     * @return \Illuminate\Http\Response
     */
    public function edit(Ruangan $ruangan)
    {
        // ambil seluruh data dari kategori-ruangan
        $kategori_ruangan = KategoriRuangan::all();
        // ambil seluruh data dari Gedung
        $gedung = Gedung::all();

        return view('ruangan.edit', compact('ruangan','kategori_ruangan','gedung'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ruangan  $ruangan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ruangan $ruangan)
    {
//--------proses update data lama & upload file foto baru--------
        $image = $request->file('foto1');
        if(!empty($image)) //kondisi akan upload foto baru
        {
            if(!empty($ruangan->foto1)){ //kondisi ada nama file foto di tabel
                //hapus foto lama
                unlink('img/ruangan/'.$ruangan->foto1);
            }
            //proses upload foto baru
            $destinationPath = 'img/ruangan/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            //print_r($profileImage); die();
            $image->move($destinationPath, $profileImage);
            $input['foto1'] = "$profileImage";
        }
        else //kondisi user hanya update data saja, bukan ganti foto
        {
            $input['foto1'] = $ruangan->foto1; //nama file foto masih yg lama
        }

        // $ruangan->update($input);

        // return redirect()->route('ruangan.index')
                        // ->with('success','Ruangan updated successfully.');
        try {
            $ruangan->update($input);

            return redirect()->route('ruangan.index')
                ->with('success', 'Edit Ruangan successfully!');
        } catch (\Exception $e){
            return redirect()->back()
                ->with('error', 'Error during the creation!');
        }

        // // Proses input data ruangan with foto
        // $request->validate([
        //     'nama' => 'required',
        //     'kategori_ruangan_id' => 'required',
        //     'gedung_id' => 'required',
        //     'kapasitas' => 'required|numeric',
        //     'lantai' => 'required|numeric',
        //     'foto1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:3068',
        // ]);
        // $image = $request->file('foto1');
        // if(!empty($image)) //kondisi akan upload foto baru
        // {
        //     if(!empty($ruangan->foto1)){ //kondisi ada nama file foto di tabel
        //         //hapus foto lama
        //         unlink('img/ruangan/'.$ruangan->foto1);
        //     }
        //     //proses upload foto baru
        //     $destinationPath = 'img/ruangan/';
        //     $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
        //     //print_r($profileImage); die();
        //     $image->move($destinationPath, $profileImage);
        //     $input['foto1'] = "$profileImage";
        // }
        // else //kondisi user hanya update data saja, bukan ganti foto
        // {
        //     $input['foto1'] = $ruangan->foto1; //nama file foto masih yg lama
        // }

        // $ruangan->update($input);

        // return redirect()->route('ruangan.index')
        //                 ->with('success','Ruangan updated successfully.')


        // $input = $request->all();

        // // logic for proses foto
        // if ($image = $request->file('foto1')) {
        //     $destinationPath = 'img/ruangan';
        //     $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
        //     $image->move($destinationPath, $profileImage);
        //     $input['foto1'] = "$profileImage";
        // }

        // // input create gedung function
        // // $ruangan = Ruangan::where('id', $ruangan->id)->update($input);
        // // Ruangan::where('id', $ruangan->id)->update($request->all());
        // $ruangan->update($request->all());


        // // kembali ke page
        // return redirect()->route('ruangan.index')
        //                 ->with('success','Gedung created successfully.');
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ruangan  $ruangan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ruangan $ruangan, $id)
    {
       //--------hapus dulu fisik file foto--------
       $ruangan = Ruangan::find($id);
       if(!empty($ruangan->foto1)) unlink('img/ruangan/'.$ruangan->foto1);
        // dd($ruangan = Ruangan::find($id));

        // Delete Data Mahasiswa
        // $ruangan->delete();
        $ruangan = Ruangan::where('id', $id)->delete();

        // return redirect()->route('ruangan.index')
        //                 ->with('success','deleted Data berhasil');

        return back()->with('success','Data deleted successfully');
    }
}
