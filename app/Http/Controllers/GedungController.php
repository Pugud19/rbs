<?php

namespace App\Http\Controllers;

// use App\Helpers\ApiFormater;
use Illuminate\Http\Request;
use App\Models\Gedung;

class GedungController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // tampil data for admin page

    public function index()
    {
        // Tampil Data from Database
        $gedungs = Gedung::all();
        return view('gedung.index',compact('gedungs'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
        // if($gedungs){
        //     return ApiFormater::createApi(200, 'success', $gedungs);
        // }else{
        //     return ApiFormater::createApi(400, 'failed');
        // }

    }

    // public function index()
    // {
    //     // Tampil Data from Database
    //     $gedungs = Gedung::latest()->paginate(5);

    //     return view('gedung.index',compact('gedungs'))
    //         ->with('i', (request()->input('page', 1) - 1) * 5);
    // }

    // tampil data for landingpage
    public function gedungList()
    {
        // Tampil Data from Database
        $gedungs = Gedung::latest()->paginate(5);

        return view('gedung.gedung-list',compact('gedungs'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Tambah ke form input data
        return view('gedung.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // try {
        // Proses input data gedung with foto
        $request->validate([
            'kode' => 'required|unique:gedung,kode|max:6',
            'nama' => 'required|max:225',
            'alamat' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $input = $request->all();

        // logic for proses foto
        if ($image = $request->file('foto')) {
            $destinationPath = 'img/gedung';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['foto'] = "$profileImage";
        }

        // input create gedung function
        $gedungs = Gedung::create($input);
        $data = Gedung::where('id', '=', $gedungs->id)->get();


        // if($data){
        //     return ApiFormater::createApi(200, 'success create data', $data);
        // }else{
        //     return ApiFormater::createApi(400, 'failed');
        // }

        // } catch (\Exception $error) {
        //     return ApiFormater::createApi(400, 'failed');
        // }

        // kembali ke page
        return redirect()->route('gedung.index')
                        ->with('success','Gedung created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // //
        // $data = Gedung::where('id', '=', $id)->get();


        // if($data){
        //     return ApiFormater::createApi(200, 'show data', $data);
        // }else{
        //     return ApiFormater::createApi(400, 'show data failed');
        // }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        // try {
        //     // Proses input data gedung with foto
        //     $request->validate([
        //         'kode' => 'required|unique:gedung,kode|max:6',
        //         'nama' => 'required|max:225',
        //         'alamat' => 'required',
        //         'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        //     ]);

        //     $gedungs = Gedung::findOrFail($id);
        //     // input create gedung function
        //     $gedungs->update($request->all());
        //     $data = Gedung::where('id', '=', $id)->get();


        //     if($data){
        //         return ApiFormater::createApi(200, 'success update data', $data);
        //     }else{
        //         return ApiFormater::createApi(400, 'failed');
        //     }

        //     } catch (\Exception $error) {
        //         return ApiFormater::createApi(400, 'failed');
        //     }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // $gedungs = Gedung::findOrFail($id);
        // $data = $gedungs->delete();

        // if($data){
        //     return ApiFormater::createApi(200, 'success Destroy data', $data);
        // }else{
        //     return ApiFormater::createApi(400, 'failed');
        // }

    }
}
