<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ApiConsumerController extends Controller
{
    //
    public $baseurl = '';

    public function __construct()
    {
        $this->baseurl = "http://cloud.producode.id:5000/api/v1/";
    }

    public function list($service){
        $resp = Http::get("$this->baseurl/$service");
        return $resp->json();
    }

    public function detail($service, $id){
        $resp = Http::get("$this->baseurl/$service/$id");
        return $resp->json();
    }
}
