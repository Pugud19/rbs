<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gedung extends Model
{
    use HasFactory;

    protected $table = 'gedung';

    protected $fillable =[
        'kode', 'nama', 'alamat', 'foto'
    ];

    // database relation one to many
    public function ruangan()
    {
        return $this->hasMany(Ruangan::class);
    }
}
