<?php

use App\Http\Controllers\ApiConsumerController;
use App\Http\Controllers\GedungController;
use App\Http\Controllers\KategoriRuanganController;
use App\Http\Controllers\FasilitasController;
use App\Http\Controllers\RuanganController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// =========== Route For Landing-page ===========
Route::view('/', 'landingpage.home');
Route::view('/home', 'landingpage.home');
Route::view('/about', 'landingpage.about');
Route::view('/services', 'landingpage.services');
Route::view('/blog', 'landingpage.blog');
Route::view('/contact', 'landingpage.contact');
Route::view('/list', 'landingpage.list');
Route::view('/aula-atas', 'landingpage.aula-atas');
Route::view('/aula-bawah', 'landingpage.aula-bawah');
Route::view('/lapangan', 'landingpage.lapangan');
Route::view('/tepas', 'landingpage.tepas');

// =========== Route For Admin ===========
Route::view('/admin', 'admin.home');
// Route::view('/peminjaman', 'admin.peminjaman');
Route::view('/admin/pages/basictable', 'admin.pages.basictable');
Route::view('/admin/pages/basicelement', 'admin.pages.basicelement');
Route::view('/admin/pages/chart', 'admin.pages.chart');
Route::view('/admin/pages/icons', 'admin.pages.icons');

// =========== Route For Bisnis Proses ===========
// Route::resource('/gedung', GedungController::class)->middleware('auth');
// Route::resource('/users', UserController::class)->middleware('auth');
// Route::resource('/fasilitas', FasilitasController::class)->middleware('auth');
// Route::resource('/ruangan', RuanganController::class)->middleware('auth');
// // Route::get('/ruangan-delete/{id}', 'RuanganController@destroy')->name('ruangans.destory');
// // Route::get('/ruangan-delete/{id}', [RuanganController::class, 'destory'] )->name('destory');
// Route::get('/ruangan-delete/{id}', 'App\Http\Controllers\RuanganController@destroy')->middleware('auth');
// Route::resource('/kategori-ruangan', KategoriRuanganController::class);
// Route::get('/gedung-list', [GedungController::class, 'gedungList']);


// // =========== Route For Export PDF ===========
// Route::get('/generate-pdf', [RuanganController::class, 'generatePDF'])->middleware('auth');
// Route::get('/ruangan-pdf', [RuanganController::class, 'ruanganPDF'])->middleware('auth');


// Route Group for every auth
Route::group(['middleware' => ['access']], function (){
Route::resource('/gedung', GedungController::class);
Route::resource('/users', UserController::class);
Route::resource('/fasilitas', FasilitasController::class);
Route::resource('/ruangan', RuanganController::class);
// Route::get('/ruangan-delete/{id}', 'RuanganController@destroy')->name('ruangans.destory');
// Route::get('/ruangan-delete/{id}', [RuanganController::class, 'destory'] )->name('destory');
Route::get('/ruangan-delete/{id}', 'App\Http\Controllers\RuanganController@destroy');
Route::resource('/kategori-ruangan', KategoriRuanganController::class);
Route::get('/gedung-list', [GedungController::class, 'gedungList']);

// =========== Route For Export PDF ===========
Route::get('/generate-pdf', [RuanganController::class, 'generatePDF']);
Route::get('/ruangan-pdf', [RuanganController::class, 'ruanganPDF']);

// api get for ruangan
// Route::get('api/{service}', [ApiConsumerController::class,'list']);
// Route::get('api/{service}/{id}', [ApiConsumerController::class,'detail']);

});
Route::get('/peminjaman', function() {
    return view('admin.peminjaman');
});



// Route cara 2
// Route::middleware(['admin', 'staff'])->group(function () {
//     Route::resource('/gedung', GedungController::class);
//     Route::resource('/users', UserController::class);
//     Route::resource('/fasilitas', FasilitasController::class);
//     Route::resource('/ruangan', RuanganController::class);
//     // Route::get('/ruangan-delete/{id}', 'RuanganController@destroy')->name('ruangans.destory');
//     // Route::get('/ruangan-delete/{id}', [RuanganController::class, 'destory'] )->name('destory');
//     Route::get('/ruangan-delete/{id}', 'App\Http\Controllers\RuanganController@destroy');
//     Route::resource('/kategori-ruangan', KategoriRuanganController::class);
//     Route::get('/gedung-list', [GedungController::class, 'gedungList']);


//     // =========== Route For Export PDF ===========
//     Route::get('/generate-pdf', [RuanganController::class, 'generatePDF']);
//     Route::get('/ruangan-pdf', [RuanganController::class, 'ruanganPDF']);
//     // Route::view('/peminjaman', 'admin.peminjaman');

// });
// for authentication user
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::view('/after-register', 'landingpage.after_register');
Route::view('/access-denied', 'access');
