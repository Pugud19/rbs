## About RBS

Trixie adalah sebuah website e-comerce sederhana dengan memiliki payment gateway memakai midtrans yang menggunakan laravel framework untuk kerangka kerja code dengan memiliki tools yang dipakai seperti:

- [Laravel Framework versi ^8.0](https://laravel.com/).
- [PHP versi ^8.0](https://www.php.net/).
- [MariaDB versi ^5.7](https://mariadb.org/).
- [JetStream versi ^2.0](https://jetstream.laravel.com/2.x/introduction.html).
- [Tailwindcss versi ^3.1](hhttps://tailwindcss.com/).
- [Bootstrap versi ^5.0](https://getbootstrap.com/).


## Instalation RBS

Untuk beberapa keperluan bagi anda yang ingin melakukan pengembangan terhadap aplikasi ini dan juga ingin memakai aplikasi ini untuk beberapa keperluan silakan ikuti langkah berikut ini :

#### clone this project
using https : 
```
git clone https://github.com/kel-1-Kampus-Merdeka/olshop-pakaian.git
```
using ssh :
```
git clone git@gitlab.com:Pugud19/online-shop.git
``` 

#### set your .env file
##### create file .env
```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:4LJo6BYLxgkCafgR13s0GTowdSPrqzlY+nWhtQZp+VM=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=rbs
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DRIVER=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=database
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

MIDTRANS_SERVER_KEY="SB-Mid-server-75fgSQZ2S0SPSVXWuzwIraMH"
MIDTRANS_CLIENT_KEY="SB-Mid-client-Q0C0mWmxeKWybmvr"
MIDTRANS_IS_PRODUCTION=false
MIDTRANS_IS_SANITIZED=true
MIDTRANS_IS_3DS=true

``` 

#### install all depedencies using npm and composer
```
composer install 
npm install && npm run dev 
npm run watch 
```

#### set your storage folder 
```
php artisan storage:link 
php artisan serve
```


### Semua Partners Pengerjaan Project

- **[Pugud Pambudi](https://www.linkedin.com/in/pugud-pambudi-b30171231/)**


## License

Open-source ini memiliki lisensi dibawah naungan [MIT license](https://opensource.org/licenses/MIT).
